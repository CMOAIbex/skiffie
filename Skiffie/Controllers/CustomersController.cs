﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;
using System.Text.RegularExpressions;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace Skiffie.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(
            new UserStore<ApplicationUser>(
                new SkiffieContext()));
        private SkiffieContext db = new SkiffieContext();

        // GET: Customers
        public ActionResult Index()
        {
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName");
            return View();
        }

        public ActionResult GetCustomerList(bool ShowActive = true, bool ShowHold = true, bool ShowCancelled = true,
            int TowerID = -1, string Name = "", string Phone = "", int skip = 0, int count = -1)
        {
            var customers = db.Customers.Include(c => c.IP).Include(c => c.Tower).OrderBy(c=>c.TowerId).ThenBy(c => c.Name).ToList();

            //now start filtering.
            if (!ShowActive)
            {
                customers = customers.Where(x => x.CustomerState != Customer.CustomerStatus.Active &&
                    x.CustomerState != Customer.CustomerStatus.Pending).ToList();
            }

            if (!ShowHold)
            {
                customers = customers.Where(x => x.CustomerState != Customer.CustomerStatus.Hold).ToList();
            }

            if (!ShowCancelled)
            {
                customers = customers.Where(x => x.CustomerState != Customer.CustomerStatus.Cancelled).ToList();
            }

            if (TowerID != -1)
            {
                customers = customers.Where(x => x.TowerId == TowerID).ToList();
            }

            if (Name != "")
            {
                customers = customers.Where(x => x.Name.ToLower().Contains(Name.ToLower())).ToList();
            }

            if (Phone != "")
            {
                string TN = Regex.Replace(Phone, @"[^\d]", "");
                customers = customers.Where(x => x._PhoneNumber.Contains(TN)).ToList();
            }

            customers = customers.Skip(skip).ToList();

            if (count > 0)
            {
                customers = customers.Take(count).ToList();
            }

            return PartialView("_CustomersTable", customers);
        }

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            //nice and handy over at http://stackoverflow.com/questions/27133014/exclude-remove-value-from-mvc-5-1-enumdropdownlistfor
            var CustomerStatusList = Enum.GetValues(typeof(Customer.CustomerStatus))
                .Cast<Customer.CustomerStatus>()
                .Where(e => e != Customer.CustomerStatus.Cancelled && e != Customer.CustomerStatus.Pending)
                .Select(e => new SelectListItem
                {
                    Value = ((int)e).ToString(),
                    Text = e.ToString()
                });

            ViewBag.CustomerState = CustomerStatusList;
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet");
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName");
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Address,PhoneNumber,PPPoEUser,PPPoEPassword,TowerId,IPID,CustomerSpeed,CustomerState")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                //only relevant if customer status is active.
                if (customer.CustomerState==Customer.CustomerStatus.Active || customer.CustomerState == Customer.CustomerStatus.Pending) {
                    db.IPs.First(x => x.IPId == customer.IPID).CustomerID = customer.CustomerID;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var CustomerStatusList = Enum.GetValues(typeof(Customer.CustomerStatus))
                .Cast<Customer.CustomerStatus>()
                .Where(e => e != Customer.CustomerStatus.Cancelled && e != Customer.CustomerStatus.Pending)
                .Select(e => new SelectListItem
                {
                    Value = ((int)e).ToString(),
                    Text = e.ToString()
                });

            ViewBag.CustomerState = CustomerStatusList;
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.Actions = ActionButtons(customer);
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
            return View(customer);
        }

        //get the buttons that you can click on the screen.
        private List<NavigationBars> ActionButtons(Customer customer)
        {
            var identity = (ClaimsIdentity)User.Identity;
            List<NavigationBars> MyActions = new List<NavigationBars>();

            var rank = identity.Claims.DefaultIfEmpty(null).FirstOrDefault(x => x.Type == ("UserRole")).Value;

            if (customer.CustomerState == Customer.CustomerStatus.Active || customer.CustomerState == Customer.CustomerStatus.Pending ||
                customer.CustomerState == Customer.CustomerStatus.Hold){
                MyActions.AddRange(new List<NavigationBars>{
                    new NavigationBars
                    {
                        Action="Cancel",
                        Name="Cancel",
                        Controller="Customers"
                    }
                });
                if (customer.CustomerState != Customer.CustomerStatus.Active)
                {
                    MyActions.Add(
                        new NavigationBars
                        {
                            Action = "Activate",
                            Name = "Activate",
                            Controller = "Customers"
                        }
                    );
                };
            }

            if (customer.CustomerState == Customer.CustomerStatus.Cancelled)
            {
                MyActions.Add(
                    new NavigationBars
                    {
                        Action = "Activate",
                        Name = "Activate",
                        Controller = "Customers"
                    }
                );
                if (rank == "Admin")
                {
                    MyActions.Add(
                    new NavigationBars
                    {
                        Action = "Delete",
                        Name = "Delete",
                        Controller = "Customers"
                    });
                }
            }

            return MyActions;
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerState,CustomerID,Name,Address,PhoneNumber,PPPoEUser,PPPoEPassword,TowerId,IPID,CustomerSpeed")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Actions = ActionButtons(customer);
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
            return View(customer);
        }

        #region cancels.

        public ActionResult Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost, ActionName("Cancel")]
        [ValidateAntiForgeryToken]
        public ActionResult CancelConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);

            var IP = db.IPs.DefaultIfEmpty(null).FirstOrDefault(x => x.CustomerID == customer.IPID);
            if (IP != null)
            {
                IP.CustomerID = null;
            }

            customer.CustomerState = Customer.CustomerStatus.Cancelled;
            customer.IPID = null;
            customer.TowerId = null;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region Activates.

        public ActionResult Activate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            if (customer.CustomerState == Customer.CustomerStatus.Active)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CustomerActivateView cx = new CustomerActivateView
            {
                ID = customer.CustomerID,
                TowerId = customer.TowerId,
                IPID = customer.IPID == null ? -1 : (int)customer.IPID,
                PhoneNumber = customer.PhoneNumber,
                Name = customer.Name
            };
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
            return View(cx);
        }

        //activate goes from cancelled to active again.
        [HttpPost, ActionName("Activate")]
        [ValidateAntiForgeryToken]
        public ActionResult ActivateConfirmed([Bind(Include = "ID, Name, PhoneNumber, TowerId, IPID")] CustomerActivateView customer)
        {
            Customer cx = db.Customers.DefaultIfEmpty(null).FirstOrDefault(x=>x.CustomerID == customer.ID);
            if (cx != null && cx.CustomerState != Customer.CustomerStatus.Active && ModelState.IsValid)
            {
                cx.Name = customer.Name;
                cx.PhoneNumber = customer.PhoneNumber;
                cx.TowerId = customer.TowerId;
                cx.IPID = customer.IPID;
                cx.CustomerState = Customer.CustomerStatus.Active;

                db.SaveChanges();

                return RedirectToAction("Index", "Customers");
            }

            if (customer.TowerId == null)
            {
                ModelState.AddModelError("", "A Tower must be selected!");
            }
            if (customer.IPID == null)
            {
                ModelState.AddModelError("", "An IP must be selected!");
            }

            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
            return View(customer);
        }

        #endregion

        #region Deletes
        // GET: Customers/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);

            var IP = db.IPs.DefaultIfEmpty(null).FirstOrDefault(x => x.CustomerID == customer.IPID);
            if (IP != null)
            {
                IP.CustomerID = null;
            }

            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CreateCustomerPartial()
        {
            ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet");
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName");

            return PartialView("_CreateCustomer");
        }

        [HttpPost]
        public ActionResult CreateCustomerPartialPost([Bind] Customer cx)
        {
            if (!ModelState.IsValid || cx.TowerId == null || cx.IPID == null)
            {
                return Json(new { error = "Please fill everything out and make sure IP isn't already assigned." });
            }
            else if (cx.TowerId == null)
            {
                return Json(new { error = "Please select a tower." });
            }
            else if (cx.IPID == null)
            {
                return Json(new { error = "Please select an IP." });
            }

            db.Customers.Add(cx);
            db.SaveChanges();
            return Json(new { cxid = cx.CustomerID });
        }

        public ActionResult PartialCurrentCustomers(string AppointmentType = "", string FilterName = "", string FilterPhone = "", int CustomerID = 0)
        {
            IEnumerable<Customer> customers;

            if (AppointmentType == "Install") {
                customers = db.Customers.Where(x => x.CustomerState == Customer.CustomerStatus.Hold && x.TowerId != null && x.IPID != null).OrderBy(x=>x.Name);
            }
            else if (AppointmentType == "Repair")
            {
                customers = db.Customers.Where(x => x.CustomerState == Customer.CustomerStatus.Active).OrderBy(x => x.Name);
            }
            else
            {
                customers = db.Customers.Where(x => x.CustomerState == Customer.CustomerStatus.Active || x.CustomerState == Customer.CustomerStatus.Hold).OrderBy(x => x.Name);
            }

            if (FilterName != "")
            {
                customers = customers.Where(x => x.Name.ToLower().Contains(FilterName.ToLower()));
            }
            if (FilterPhone != "")
            {
                customers = customers.Where(x => x._PhoneNumber.Contains(FilterPhone) || x.PhoneNumber.Contains(FilterPhone));
            }

            List<object> newList = new List<object>();
            foreach (var cx in customers)
            {
                newList.Add(new
                {
                    Id = cx.CustomerID,
                    Customer = cx.Name + " " + cx.PhoneNumber
                });
            }

            ViewBag.cx = new SelectList(newList, "Id", "Customer", CustomerID);

            return PartialView("_CurrentCustomers", customers);
        }

        public ActionResult UpdateIPs(int id, int customerid = -1)
        {

            //look into a view mdoel later.
            var thisTower = db.Towers.First(x => x.TowerID == id);
            ViewBag.Subnet = thisTower.Subnet;
            //get the customer list, too, for comparisons for IPs.
            List<IP> IP = db.IPs.Where(x => x.TowerId == id && (x.CustomerID == null || x.CustomerID == customerid) && x.CustomerUseable).ToList();

            var Customers = db.Customers.Where(x => x.TowerId == id);
            foreach (var cx in Customers)
           { 
                if (cx.IPID != null && cx.CustomerID != customerid){
                    var myIP = IP.DefaultIfEmpty(null).FirstOrDefault(x => x.IPId == cx.IPID);
                    if (myIP != null)
                    {
                        IP.Remove(myIP);
                    }
                }                
            }

            int? selectedIP;

            if (customerid == -1)
            {
                selectedIP = null;
            }
            else
            {
                selectedIP = db.Customers.First(x => x.CustomerID == customerid).IPID;
            }

            var IPs = IP.OrderBy(g => g.IPAddress);

            List<object> newList = new List<object>();
            foreach (var ip in IPs)
            {
                newList.Add(new
                {
                    Id = ip.IPId,
                    IPAddress = ip.Subnet + "." + ip.IPAddress
                });
            }

            ViewBag.IP = new SelectList(newList, "Id", "IPAddress", selectedIP);
            ViewBag.SelectedIP = selectedIP;
            return PartialView("_IPList", IP);
        }

        //pulls up appropriate actions based on the state the customer is in.
        public ActionResult IndexActivities(int id)
        {
            var IndexActivities = new List<CustomerIndexActivitiesView>();
            var customerState = db.Customers.First(x => x.CustomerID == id).CustomerState;

            if (customerState == Customer.CustomerStatus.Active)
            {
                IndexActivities.AddRange(new List<CustomerIndexActivitiesView> {
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Edit",
                        ActivityName = "Edit"
                    },
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Cancel",
                        ActivityName = "Cancel"
                    }
                });
            }
            else if (customerState == Customer.CustomerStatus.Cancelled)
            {
                IndexActivities.AddRange(new List<CustomerIndexActivitiesView> {
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Activate",
                        ActivityName = "Activate"
                    },
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Delete",
                        ActivityName = "Delete"
                    }
                });
            }
            else if (customerState == Customer.CustomerStatus.Hold || customerState == Customer.CustomerStatus.Pending)
            {
                IndexActivities.AddRange(new List<CustomerIndexActivitiesView> {
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Edit",
                        ActivityName = "Edit"
                    },
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Activate",
                        ActivityName = "Activate"
                    },
                    new CustomerIndexActivitiesView{
                        CustomerID = id,
                        Activity = "Cancel",
                        ActivityName = "Cancel"
                    }
                });
            }
            return PartialView("_IndexActivities", IndexActivities);
        }

        //gets the customer information for appointment views. they should be of limited editing abiltiies.
        public ActionResult CustomerView(int? CustomerID)
        {
            if (CustomerID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.DefaultIfEmpty(null).FirstOrDefault(x => x.CustomerID == CustomerID);
            if (customer == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var myRole = db.SkiffieRoles.Find(manager.FindById(User.Identity.GetUserId()).SkiffieRoleId).Name;
            if (myRole == "Agent")
            {
                ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
                ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
                return PartialView("_appointmentCustomer", customer);
            }
            else
            {
                ViewBag.IPID = new SelectList(db.IPs, "IPId", "Subnet", customer.IPID);
                ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName", customer.TowerId);
                return PartialView("_appointmentCustomerTech", customer);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateTowerAndIP(int CustomerID, int TowerId, int IPId)
        {
            var cx = db.Customers.Find(CustomerID);
            if (cx == null)
            {
                return Json(new { error = "No customer found!" });
            }
            var tower = db.Towers.Find(TowerId);
            if (tower == null)
            {
                return Json(new { error = "No tower found!" });
            }
            var ip = tower.IPs.DefaultIfEmpty(null).FirstOrDefault(x=>x.IPId == IPId);
            if (ip == null)
            {
                return Json(new { error = "Invalid IP." });
            }
            cx.TowerId = TowerId;
            cx.IPID = IPId;
            db.SaveChanges();
            return Json(new { success = "Successfully changed!" });
        }

        [Authorize( Roles = "Admin")]
        public ActionResult MassAdd()
        {
            ViewBag.TowerId = new SelectList(db.Towers, "TowerID", "TowerName");
            return View();
        }


        [HttpPost, ActionName("MassAdd")]
        [ValidateAntiForgeryToken]
        [Authorize( Roles = "Admin")]
        public ActionResult MassAddPush(int TowerID, string Data)
        {
            string[] Seperator = new string[]{"\r\n"};
            string[] result;

            result = Data.Split(Seperator, StringSplitOptions.None);

            for (int i = 0; i < result.Length; i++)
            {
                //split the value.
                string[] subResult = result[i].Split(';');
                byte ip = new Byte();
                Byte.TryParse(subResult[0], out ip);
                //first check to see that this ip is not blocked... if it is, ignore it. 
                if (db.Towers.Find(TowerID).IPs.First(x => x.IPAddress == ip).CustomerUseable)
                {
                    //Then, check to see if a customer is using it. Ignore it if so.
                    if (db.Customers.Where(x => x.TowerId == TowerID && x.IP.IPAddress == ip).Count() == 0)
                    {
                        db.Customers.Add(new Customer
                        {
                            Name = "ZZ PH " + subResult[1],
                            CustomerSpeed = Customer.Speed.Basic,
                            Address = "ph",
                            PhoneNumber = "1111111111",
                            CustomerState = Customer.CustomerStatus.Active,
                            IPID = db.Towers.Find(TowerID).IPs.First(x => x.IPAddress == ip).IPId,
                            PPPoEPassword = "ph",
                            PPPoEUser = "ph",
                            TowerId = TowerID,
                        });
                    }
                }
            }
            db.SaveChanges();

            return RedirectToAction("MassAdd");
        }
    }
}