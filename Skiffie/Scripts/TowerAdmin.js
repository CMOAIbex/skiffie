﻿$(document).ready(function () {
    $(".ChangeAPState").change(function () {
        $("#messages").text("Loading...");
        $.ajax({
            url: '/Towers/UpdateAPState/',
            data: { id: $(this).attr("apid"), value: $(this).val() },
            contentType: 'application/HTML; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function () {
            $("#messages").html("Succesful change!");
        })
        .fail(function () {
            $("#messages").html("Error.");
        });
    });

    $("#ManageIPButton").click(function () {
        $(".IPManager").show("slow");
        UpdateBlockedIPList();
    });

    $("#StartIP").change(function () {
        $(this).val($(this).val().replace(/[^\d]/g, ""));
        $(this).val(Math.max(Math.min(255, $(this).val()), 1));
        if ($("#EndIP").val() == "") {
            $("#EndIP").val($(this).val());
        }
        if ($("#EndIP").val() < $(this).val()) {
            $("#EndIP").val($(this).val());
        }
    });

    $("#EndIP").change(function () {
        $(this).val($(this).val().replace(/[^\d]/g, ""));
        if ($("#StartIP").val() == "") {
            $("#StartIP").val($(this).val());
        }
        $(this).val(Math.max(Math.max(Math.min(255, $(this).val()), 1), $("#StartIP").val()));
    });

    $("#BlockIPs").click(function () {
        if ($("#StartIP").val() != "" && $("EndIP").val() != "") {
            $.post("/IPs/IPBlock",
                $(".IPManage").serialize(),
                function (data) {
                    if(data["error"] != null){
                        $("#IPManagerMessage").html(data["error"]);
                    } else {
                        $("#IPManagerMessage").html(data["result"]);
                        UpdateBlockedIPList();
                    }
                });
        };
    });

    $("#AllowIPs").click(function () {
        if ($("#StartIP").val() != "" && $("EndIP").val() != "") {
            $.post("/IPs/IPPermit",
                $(".IPManage").serialize(),
                function (data) {
                    if (data["error"] != null) {
                        $("#IPManagerMessage").html(data["error"]);
                    } else {
                        $("#IPManagerMessage").html(data["result"]);
                        UpdateBlockedIPList();
                    }
                });
        };
    });
});

function UpdateBlockedIPList() {
    $("#CurrentBlockedIPs").html("Loading...");
    $.get("/IPs/GetBlockedIPs", $("#TowerEditID").serialize(),
        function (data) {
            $("#CurrentBlockedIPs").html(data);
        });
}