﻿$(document).ready(function () {
    $("#AddAP").click(function () {
        $.ajax({
            url: "/Towers/APCreate",
            type: 'GET',
            contentType: 'application/html; charset=utf-8',
            dataType: 'html'
        })
        .success(function (data) {
            $("#APs").append(data);
        });
    });

    $("body").on('click', ".RemoveAP", function () {
        $(this).closest(".APParentSpan").remove();
    });

    $("body").on('click', ".RemoveExistingAP", function () {
        if ($(this).text() != "Confirm Removal") {
            $(this).text("Confirm Removal");
        } else {
            var parent = $(this).closest(".APParentSpan");
            var id = parent.children(".thisAPId").val();
            $.ajax({
                url: "/Towers/RemoveExistingAP",
                data: { id: id },
                type: 'GET',
                contentType: 'application/html; chartset=utf-8',
                dataType: 'html'
            }).success(function (data) {
                parent.remove();
            });
        }
    });
});