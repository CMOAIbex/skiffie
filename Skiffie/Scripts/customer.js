﻿$(document).ready(function () {
    TowerChange($("#TowerId"));

    $("#FilterCustomerList").click(function () {
        PopulateCustomerListDivTable();
    });

    if ($("#CustomersListDiv").length > 0) {
        PopulateCustomerListDivTable();
    }

    $("#CreateCustomerDialog").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 300
        },
        hide: {
            effect: "blind",
            duration:200
        },
        model: true,
        title: "Create Customer",
        width: 800,
        height:500
    });

    $("#CreateCustomerDialog #CreateCustomerFromPartial").click(function () {
        $.post('/Customers/CreateCustomerPartialPost',
            $("#CustomerForm").serialize(),
            function (data) {
                if (data["error"] != null) {
                    $("#CustomerValidationSummary").html(data["error"]);
                } else {
                    $("#CustomerValidationSummary").html("");
                    $("#CreateCustomerDialog").dialog("close");
                    UpdateCustomersCreate(data["cxid"]);
                }
            }
        );
    });

    $("#OpenCreateCustomer").click(function () {
        $("#CreateCustomerDialog").dialog("open");
    });

    $("#IPId").change(function () {
        //alert($(this).val())
    });
    
    $("#Active").change(function () {
        if ($(this).prop('checked')) {
            $("#IPField").show();
        } else {
            $("#IPField").hide();
            $("#TowerField").hide();
        }
    })

    $("#TowerId").change(function () {
        TowerChange($(this));
    });

    $(".customerListFilter").change(function () {
        $("#FilterCustomerList").trigger("click");
    });

    $("#CreateCustomerDialog #TowerId").change(function () {
        TowerChange($(this));
    });

    $("#AppointmentType").change(function () {
        UpdateCustomersCreate();
    });

    $("body").on('click tap', '.CustomerRow', function (data) {
        window.location.href = "/Customers/Edit/" + $(this).attr("data-customerid");
    });

    $(".customerActionButton").click(function () {
        window.location.href = "/Customers/" + $(this).attr("data-action") + "/" + $(this).attr("data-customerid");
    });

    function LoadIPs(towerid, customerid) {
        var customerID = -1;
        if ($("#CustomerID").length > 0 && customerid === undefined && ($("#NewCustomer").prop('checked')==false || $("#NewCustomer").length < 1)) {
            customerID = $("#CustomerID").val();
        } else if (customerid !== undefined) {
            customerID == customerid;
        }

        $.ajax({
            url: '/Customers/UpdateIPs/',
            data: { id: towerid, customerid: customerID },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#IPField").html(result);
        })
        .error(function (xhr, status) {
            //alert(status);
        });
    }

    function TowerChange(selector) {
        var active = false;
        if ($("#Active").length == 0) {
            active = true;
        } else if ($("#Active").prop('checked')) {
            active = true;
        }
        if (active) {
            if (selector.val() == "") {
                $("#IPField").html("");
            } else {
                $("#IPField").html("Loading...");
                LoadIPs(selector.val());
            }
        }
    }

    function PopulateCustomerListDivTable() {
        $.get("/Customers/GetCustomerList", $("#CustomerFilter").serialize(),
            function (data) {
                $("#CustomersListDiv").html(data);
            })
    }
});